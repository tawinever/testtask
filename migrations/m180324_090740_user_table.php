<?php

use yii\db\Migration;

/**
 * Class m180324_090740_user_table
 */
class m180324_090740_user_table extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('user',[
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'login' => $this->string()->notNull(),
            'password' => $this->string()->notNull(),
            'auth_key' => $this->string(),
        ],$tableOptions);

        $this->createIndex('login_user', 'user', 'login', true);

        $this->insert('user',[
            'name' => 'Администратор',
            'login' => 'admin',
            'auth_key' => '',
            'password' => 'admin',
        ]);

    }

    public function down()
    {
         return $this->dropTable('user');
    }

}
